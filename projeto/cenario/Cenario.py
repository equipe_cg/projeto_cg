#encoding: utf-8
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
from PIL import Image, ImageFilter
from objetos.vigilancia import *
from objetos.cerca import *

quad = gluNewQuadric()
vigilancia = Vigilancia()
cerca = Cerca()

def carrega_imagem():
    global textura1

    im = Image.open("textura/snow_2.tga", "r")
    try:
        ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBA", 0, -1)
    except SystemError:
        ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBX", 0, -1)

    textura1 = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, textura1)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)  #repetir textura na horiz
    #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT) #repetir textura na vertical
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL) # somente textura
    #glTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE) # textura + cor

    glTexImage2D(
      GL_TEXTURE_2D, 0, 3, ix, iy, 0,
      GL_RGBA, GL_UNSIGNED_BYTE, image
      )

def carrega_imagem2():
    global textura1

    im = Image.open("textura/sky.tga", "r")
    try:
        ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBA", 0, -1)
    except SystemError:
        ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBX", 0, -1)

    textura1 = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, textura1)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)  #repetir textura na horiz
    #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT) #repetir textura na vertical
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL) # somente textura
    #glTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE) # textura + cor

    glTexImage2D(
      GL_TEXTURE_2D, 0, 3, ix, iy, 0,
      GL_RGBA, GL_UNSIGNED_BYTE, image
      )


def montanhas():

    glPushMatrix()
    glTranslate(-22.0, -2.0, 25.0)
    glRotate(45, 1.0, 1.0, 1.0)
    gluQuadricNormals(quad, GLU_SMOOTH)
    gluQuadricTexture(quad, textura1)
    gluSphere(quad, 10, 5, 5)
    glPopMatrix()

    glPushMatrix()
    glTranslate(-22.0, 0.0, 0.0)
    glRotate(90, 1.0, 1.0, 1.0)
    gluQuadricNormals(quad, GLU_SMOOTH)
    gluQuadricTexture(quad, textura1)
    gluSphere(quad, 10, 5, 3)
    glPopMatrix()

    glPushMatrix()
    glTranslate(-22.0, 0.0, 10.0)
    glRotate(45, 1.0, 1.0, 1.0)
    gluQuadricNormals(quad, GLU_SMOOTH)
    gluQuadricTexture(quad, textura1)
    gluSphere(quad, 9, 5, 8)
    glPopMatrix()

    glPushMatrix()
    glTranslate(-22.0, -2.0, -10.0)
    glRotate(45, 1.0, 1.0, 1.0)
    gluQuadricNormals(quad, GLU_SMOOTH)
    gluQuadricTexture(quad, textura1)
    gluSphere(quad, 10, 5, 5)
    glPopMatrix()

    glPushMatrix()
    glTranslate(-22.0, 0.0, -20.0)
    glRotate(45, 1.0, 1.0, 1.0)
    gluQuadricNormals(quad, GLU_SMOOTH)
    gluQuadricTexture(quad, textura1)
    gluSphere(quad, 10, 5, 5)
    glPopMatrix()

    glPushMatrix()
    glTranslate(-22.0, 0.0, -35.0)
    glRotate(45, 1.0, 1.0, 1.0)
    gluQuadricNormals(quad, GLU_SMOOTH)
    gluQuadricTexture(quad, textura1)
    gluSphere(quad, 9, 5, 8)
    glPopMatrix()


def desenhor():
    global textura1, quad, cerca
    carrega_imagem2()

    #glColor3f(0.6, 0.9, 0.3) # cor RGB

    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, textura1)

    ######### Ceu ###############
    glPushMatrix()
    glRotate(-40, 1.0, 0.0, 0.0)
    gluQuadricNormals(quad, GLU_SMOOTH)
    gluQuadricTexture(quad, textura1)
    gluSphere(quad, 250, 20, 20)
    glPopMatrix()

    ########### chao ###############
    carrega_imagem()
    glPushMatrix()
    glTranslate(-20.0, -0.7, -20.0)
    glRotate(90, 1.0, 0.0, 0.0)
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, textura1)
    glBegin(GL_QUADS)
    glTexCoord2f(0.0, 0.0)
    glVertex2f(0.0, 0.0)
    glTexCoord2f(0.0, 50.0)
    glVertex2f(0.0, 50.0)
    glTexCoord2f(70.0, 50.0)
    glVertex2f(70.0, 50.0)
    glTexCoord2f(70.0, 0.0)
    glVertex2f(70.0, 0.0)
    glEnd()
    glPopMatrix()

    ########## Montanhas ##############
    glPushMatrix()
    glTranslate(0.0, 0.0, 10.0)
    montanhas()
    glPopMatrix()

    glPushMatrix()
    glTranslate(15.0, 0.0, 10.0)
    glRotate(90, 0.0, 1.0, 0.0)
    montanhas()
    glPopMatrix()

    glPushMatrix()
    glTranslate(15.0, 0.0, -45.0)
    glRotate(90, 0.0, 1.0, 0.0)
    montanhas()
    glPopMatrix()

    glDisable(GL_TEXTURE_2D)

    glPushMatrix()
    glTranslate(15.0, 0, 7.4)
    vigilancia.desenhar()
    glPopMatrix()

    glPushMatrix()
    glTranslate(15.0, 0, 2.5)
    vigilancia.desenhar()
    glPopMatrix()

    ######## cerca_direita ########
    glPushMatrix()
    glTranslate(-12.0, -0.4, -7.0)
    for i in range(0, 9):
        cerca.desenhar()
        glTranslate(3.0, 0.0, 0.0)
    glPopMatrix()

    ######## cerca_esquerda ########
    glPushMatrix()
    glTranslate(-12.0, -0.4, 17.0)
    for i in range(0, 9):
        cerca.desenhar()
        glTranslate(3.0, 0.0, 0.0)
    glPopMatrix()

    ######## cerca_tras ########
    glPushMatrix()
    glTranslate(-12.0, -0.4, 17.0)
    glRotate(90, 0.0, 1.0, 0.0)
    for i in range(0, 8):
        cerca.desenhar()
        glTranslate(3.0, 0.0, 0.0)
    glPopMatrix()

    ######## cerca_frente ########
    glPushMatrix()
    glTranslate(15.0, -0.4, 17.0)
    glRotate(90, 0.0, 1.0, 0.0)
    for i in range(0, 3):
        cerca.desenhar()
        glTranslate(3.0, 0.0, 0.0)
    glPopMatrix()

    ######## cerca_frente ########
    glPushMatrix()
    glTranslate(15.0, -0.4, 2.0)
    glRotate(90, 0.0, 1.0, 0.0)
    for i in range(0, 3):
        cerca.desenhar()
        glTranslate(3.0, 0.0, 0.0)
    glPopMatrix()
