from OpenGL.GL import glTranslate, glRotate, glScale
from OpenGL.GLU.quadrics import gluNewQuadric
from OpenGL.raw.GL.VERSION.GL_1_1 import glPushMatrix, glPopMatrix, glColor3f
from OpenGL.raw.GLU import gluQuadricDrawStyle, gluSphere
from OpenGL.raw.GLU.constants import GLU_FILL, GLU_LINE, GLU_POINT
import random

__author__ = 'allan'

class Explosao(object):
    posicao=[0,0,0]
    velocidade=1
    animacao=1
    tamanho=400

    def __init__(self,velocidade,posicao):
        self.animacao=1
        self.posicao=posicao
        self.velocidade=velocidade
        self.tamanho=400
        
    def desenhar(self):
        glPushMatrix()
        glTranslate(self.posicao[0],self.posicao[1],self.posicao[2])

        if self.animacao < self.tamanho:
            self.sphere(self.tamanho,self.tamanho/5.0)

        glPopMatrix()

    def sphere(self,limite,velocidade):
        glPushMatrix()
        self.animacao+=0.2*velocidade
        for c in range(0,15):
            glRotate(self.animacao,1,1,1)
            glPushMatrix()
            glColor3f(random.randint(900,1000)/1000.0,random.randint(500,1000)/1000.0,random.randint(0,400)/1000.0)
            glScale((random.randint(int(self.animacao),limite+10)/300.0)*self.animacao/40,(random.randint(int(self.animacao),limite+10)/300.0)*self.animacao/40,(random.randint(int(self.animacao),limite+10)/300)*self.animacao/40)
            quad=gluNewQuadric()
            gluQuadricDrawStyle(quad,GLU_POINT)
            gluSphere(quad,1,int(self.animacao)+1,int(self.animacao)+1)
            glPopMatrix()
        glPopMatrix()