# -*-coding: utf-8 -*
#mary
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

class Barricada(object):

        def desenhar(self):
                glColor(0.2,.3,.1)
                glPushMatrix()
                glRotate(90,0,1,0)
                self.madeira(-1,0,-2.25,4,30,30)
                glPopMatrix()
                glPushMatrix()
                self.lado1(0,0,0)
                glTranslate(0,0.9,1)
                self.lado1(1,0,0)
                glPopMatrix()

        def madeira(self, x,y,z, e, f, g):
                glTranslate(x,y,z)
                glPushMatrix()
                glutSolidCylinder(0.06, e, f, g)
                glPopMatrix()

        def lado1(self,x,y,z):
                glRotate(90,x,y,z)
                self.menor(0.3,0.3,-2.2,1,0,0)
                self.menor(-2.8,0.3,-2.2,1,0,0)

        def menor(self,x,y,z,a,b,c):
                glPushMatrix()
                glTranslate(x,y,z)
                glRotate(45,a,b,c)
                self.madeira(1,2,2,1,30,30)
                glPopMatrix()

