from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

class Vigilancia(object):

    def haste(self):
        glPushMatrix()
        glColor3f(190/255.0, 190/255.0, 190/255.0)
        glRotate(90, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.02, 0.7, 20, 20)
        glPopMatrix()
    def teto(self):
        glPushMatrix()
        glColor3f(110/255.0, 123/255.0, 139/255.0)
        glScale(1.0, 0.02, 0.63)
        glutSolidCube(1)
        glPopMatrix()
    def desenhar(self):
        glPushMatrix()
        glColor3f(110/255.0, 123/255.0, 139/255.0)
        glTranslate(0.0, 1.0, 0.0)
        glScale(1.2, 0.5, 1.2)
        glutSolidCube(1)
        glPopMatrix()

        ######### Hastes ###########
        glPushMatrix()
        glTranslate(0.4, 1.92, 0.5)
        self.haste()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.4, 1.92, -0.5)
        self.haste()
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.4, 1.92, 0.5)
        self.haste()
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.4, 1.92, -0.5)
        self.haste()
        glPopMatrix()
        ###########################

        ####### Teto #############
        glPushMatrix()
        glTranslate(0.0, 2.0, 0.3)
        glRotate(20, 1.0, 0.0, 0.0)
        self.teto()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, 2.0, -0.3)
        glRotate(-20, 1.0, 0.0, 0.0)
        self.teto()
        glPopMatrix()


        ##########################


        glPushMatrix()
        glColor3f(190/255.0, 190/255.0, 190/255.0)
        glScale(1.0, 2.0, 1.0)
        glutSolidCube(1)

        glPopMatrix()