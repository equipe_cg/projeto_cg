from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

class Torre:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def piso(self, x, y, z):
        glPushMatrix()
        glColor3f(110/255.0, 123/255.0, 139/255.0)
        glTranslate(x, y, z)
        glScale(6.0, 0.2, 6.0)
        glutSolidCube(0.2)
        glPopMatrix()
    def andarTerreo(self):
        glPushMatrix()
        glColor3f(190/255.0, 190/255.0, 190/255.0)
        glutSolidCube(0.95)
        glPopMatrix()
    def andarSuperior(self):
        glPushMatrix()
        glColor3f(190/255.0, 190/255.0, 190/255.0)
        glTranslate(-0.2, 1.0, 0.0)
        glScale(2.0, 5.0, 3.0)
        glutSolidCube(0.2)
        glPopMatrix()
    def teto(self, altura):
        glPushMatrix()
        glColor3f(110/255.0, 123/255.0, 139/255.0)
        glTranslate(-0.2, altura, 0.0)
        glScale(3.0, 0.2, 4.0)
        glutSolidCube(0.2)
        glPopMatrix()
    def antena(self):
        glPushMatrix()
        glColor3f(124/255.0, 155/255.0, 155/255.0)
        glTranslate(-0.25, 2.5, -0.12)
        glRotate(20, 1.0, 1.0, 0.0)
        glutWireCone(0.2, 0.15, 20, 40)
        glPopMatrix()

        glPushMatrix()
        glColor3f(105/255.0, 105/255.0, 105/255.0)
        glTranslate(-0.2, 1.8, 0.0)
        glRotate(-90, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.01, 1.0, 40, 40)
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.2, 1.8, 0.0)
        glRotate(-90, 1.0, 0.0, 0.0)
        glutWireCone(0.15, 0.7, 4, 5)
        glPopMatrix()
    def grades(self):
        glPushMatrix()
        glColor3f(139/255.0, 139/255.0, 131/255.0)
        glTranslate(-0.38, 1.5, 0.28)
        glRotate(-90, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.01, 0.2, 40, 40)
        glTranslate(0.37, 0.0, 0.0)
        glutSolidCylinder(0.01, 0.2, 40, 40)
        glTranslate(-0.38, 0.55, 0.0)
        glutSolidCylinder(0.01, 0.2, 40, 40)
        glTranslate(0.37, 0.0, 0.0)
        glutSolidCylinder(0.01, 0.2, 40, 40)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, 0.55, 0.0)
        glScale(1.0, 0.1, 1.0)
        glutWireCube(1.1)
        glTranslate(0.0, 0.25, 0.0)
        glutWireCube(1.1)
        glTranslate(0.0, 0.4, 0.0)
        glutWireCube(1.1)
        glPopMatrix()
    def desenhar(self):
        glPushMatrix()
        self.piso(0.0, -0.5, 0.0)
        self.andarTerreo()
        self.piso(0.0, 0.5, 0.0)
        self.andarSuperior()
        self.teto(1.3)
        self.teto(1.7)
        glPushMatrix()
        glTranslate(0.0, -0.1, 0.0)
        self.antena()
        glPopMatrix()
        self.grades()
        glPopMatrix()