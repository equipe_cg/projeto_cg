#!/usr/bin/env python
# -*- coding: utf-8 -*-
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

class Trem(object):
    x1 = 0
    y= 0
    rodando = 0
    def __init__(self):
        self.x1 = 0
        self.habilitar = False #n é pra desenhar o trem ainda, só quando destruir o tanque

    def desenhar(self):
        glTranslate(0, 0, self.x1)
        glPushMatrix()
        self.corpo()
        self.rodas(1, 0, 0)
        self.rodas(0.6,0,-7)
        glPopMatrix()
        self.traseira()

    def rss(self,a,b,c,d,e,f,g,h):
        glRotate(a,b,c,d)
        glScale(e,f,g)
        glutSolidCube(h)

    def tranlateCilinder(self, x,y,z,a,b,c,d):
        glTranslate(x,y,z)
        glutSolidCylinder(a,b,c,d)

    def trtss(self,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o):
        glTranslate(a,b,c)
        glRotate(d,e,f,g)
        glutSolidTorus(h, i, j, k)
        glutSolidCylinder(l, m, n, o)

    def tes(self, a,b,c,d,e,f,g):
        glTranslate(a,b,c)
        glScale(d,e,f)
        glutSolidCube(g)

    def traseira(self):
        glTranslate(0,0.2,0)
        glPushMatrix()
        self.carga(0.7,0.4,-3.5)
        self.carga(-1.3,0,0)
        glRotate(90,0,0,1)
        glScale(1,0.8,1)
        self.carga(-0.8,-0.65,0)
        glPopMatrix()
        glPushMatrix()
        self.trtss(0,-0.5,-4.5,0,0,0,0,0,0,0,0,0.3, 2.5, 30, 4)
        glPopMatrix()
        glPushMatrix()
        glRotate(90,1,0,0)
        self.trasVagao(-1.3,-0.3,-0.4)
        self.trasVagao(0,-3.2,0)
        glPopMatrix()
        glPushMatrix()
        glTranslate(0,-0.4,-1.75)
        glColor(0,0,0)
        self.tes(0,0,0,0.5,0.5,4,0.1)
        glPopMatrix()
        glTranslate(0,-1,0)

    def trem_andar(self, valor):
        if self.x1<55:
            self.x1 += valor/2
        else:
            self.x1=0
        return self.x1

    def retaDasRodas(self):
        glPushMatrix()
        glColor(0.2, 0.2, 0.2)
        self.rss(-80, 1, 0, 0,0.08, 1, 0.08,0.7)
        glPopMatrix()

    def rodas(self, x,y,z):
        glTranslate(x,y,z)
        glColor(0, 0, 0)
        glTranslate(0, -0.05, 0)
        glPushMatrix()
        self.roda(0,0,0)
        self.roda(0, 0, 0.7)
        self.roda(-.6, 0, 0)
        self.roda(0, 0, -0.7)
        glPopMatrix()
        self.rodaBig(0, 0.06, 1.5)
        self.rodaBig(-0.6, 0, 0)
        self.rodaMenor(0.6, 0, 0.8)
        self.rodaMenor(-0.6, 0, 0)
        glPushMatrix()
        glPushMatrix()
        self.girar(-0.76, -0.54, 0,0,0)
        glRotate(-15, 1, 0, 0)
        self.girar(-0.76, -2,0, 0.55, 0)
        glPopMatrix()
        glPushMatrix()
        self.girar(-0.76, -2,-0.76, 0, 1.45)
        glTranslate(0, 0.55, -1.7)
        glRotate(-15, 1, 0, 0)
        self.girar(-0.76, -2,0,0,0)
        glPopMatrix()
        glPopMatrix()

    def rodaMenor(self,x,y,z):
        glTranslate(x,y,z)
        glPushMatrix()
        self.trtss(-0.7, -.76, -0.14,90, 0, 1, 0,0.04, 0.25, 40, 40,0.07, 0.01, 30, 40)
        glRotate(self.rodando, 0, 0, 1)
        temp = 0
        while temp < 8:
            self.raio(-0.23,0.5)
            glRotatef(22.5, 0.0, .0, 1.0)
            temp = temp + 1
        glPopMatrix()

    def rodaBig(self,x,y,z):
        glTranslate(x,y,z)
        glPushMatrix()
        self.trtss(-0.7, -.66, -0.04,90, 0, 1, 0,0.04, 0.36, 100, 100,0.09, 0.01, 30, 40)
        glRotate(self.rodando, 0, 0, 1)
        temp = 0
        while temp < 8:
            self.raio(-0.4,0.8);
            glRotatef(22.5, 0.0, .0, 1.0)
            temp = temp + 1
        glPopMatrix()

    def raio(self, x,y):
        glPushMatrix()
        glRotatef(90, 1.0, 0.0, 0.0)
        self.tranlateCilinder(0.0, 0.0, x,0.009, y, 5, 5)
        glPopMatrix()

    def roda(self,x,y,z):
        glTranslate(x,y,z)
        glPushMatrix()
        self.trtss(-0.7, -.66, 0,90, 0, 1, 0,0.04, 0.3, 100, 100,0.07, 0.01, 30, 40)
        temp = 0
        glRotate(self.rodando, 0, 0, 1)
        while temp < 8:
            self.raio(-0.3,0.6);
            glRotatef(22.5, 0.0, .0, 1.0)
            temp = temp + 1
        glPopMatrix()

    def frente(self):
        glPushMatrix()
        self.tes(0,0,0,1, 0.05, 0.7,0.9)
        glPopMatrix()
        glPushMatrix()
        glRotate(50, 1, 0, 0)
        self.tranlateCilinder(0.4, 0.2, -0.55,0.01, 0.7, 30, 30)
        self.trtss(-0.8, 0, 0,0,0,0,0,0,0,0,0,0.01, 0.7, 30, 30)
        glPopMatrix()
        glPushMatrix()
        glColor(0.1, 0.1, 0.1)
        self.tes(0, -0.03, 0.3,0.95, 0.2, 0.05,1)
        glPopMatrix()
        glPushMatrix()
        glColor(0.1, 0.1, 0.1)
        glTranslate(0.5, -0.028, 0.2)
        glRotate(45, 0, 1, 0)
        self.tes(0,0,0,0.25, 0.2, 0.05,1)
        glPopMatrix()
        glPushMatrix()
        glColor(0.1, 0.1, 0.1)
        glTranslate(-0.5, -0.028, 0.2)
        glRotate(-45, 0, 1, 0)
        self.tes(0,0,0,0.25, 0.2, 0.05,1)
        glPopMatrix()

    def corpo(self):
        glColor3f(0.1, 0.1, 0.1)
        glPushMatrix()
        glutSolidCylinder(0.4, 2, 30, 4)
        glRotate(90, 1, 0, 0)
        self.cilindroMenor(0, 0.4, -0.6)
        self.cilindroMenor(0, 0.45, 0)
        self.cilindroMenor(0, 0.45, 0)

        self.cilindroMenorFino(0, 0.45, -0.2)

        glTranslate(0, 0.42, 0.3)
        self.farol()

        # esfera de dentro
        glTranslate(0, -0.28, 0.5)
        glColor(0.8, 0.1, 0.1)
        glutSolidSphere(0.3, 40, 30)

        # vagao
        glTranslate(0, -1.9, -0.35)
        self.vagao()
        glPopMatrix()

        #cilindro do lado direito
        glPushMatrix()
        glRotate(90, 0, 0, 1)
        glTranslate(0.3, -0.4, 0.25)
        glRotate(-45, 0, 0, 1)
        self.cilindroFininho()
        glPopMatrix()

        #cilindro do lado esquerdo
        glPushMatrix()
        glRotate(180, 0, 0, 1)
        glTranslate(0.4, -0.3, 0.3)
        glRotate(-45, 0, 0, 1)
        self.cilindroFininho()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0, -0.45, 2.17)
        self.frente()
        glPopMatrix()

        self.lados()

    def lados(self):
        glPushMatrix()
        glTranslate(0.43, -0.25, 1)
        glScale(0.15, 0.03, 1)
        glutSolidCube(1.5)
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.43, -0.25, 1)
        glScale(0.15, 0.03, 1)
        glutSolidCube(1.5)
        glPopMatrix()

    def cilindroMenor(self, x,y,z):
        glColor3f(0.2, 0.2, 0.2)
        glTranslate(x,y,z)
        glPushMatrix()
        glScale(2, 1.5, 1)
        glutSolidCylinder(0.1, 0.3, 30, 4)
        glPopMatrix()

    def cilindroMenorFino(self, x,y,z):
        glTranslate(x,y,z)
        # corpo do cilindro
        glColor3f(0.2, 0.2, 0.2)
        glPushMatrix()
        glutSolidCylinder(0.1, 0.7, 30, 4)
        glPopMatrix()
        # disco
        glColor3f(0.5, 0.5, 0.5)
        glPushMatrix()
        self.trtss(0, 0, 0.04,0,0,0,0,0.016, 0.12, 5, 30,0,0,0,0)
        glPopMatrix()

    def farol(self):
        glPushMatrix()
        glColor(0.3, 0.3, 0.3)
        self.trtss(0,0,0,90, 1, 0, 0,0,0,0,0,
                   0.13, 0.17, 30, 4)
        glColor(0.8, 0.8, 0.1)
        glTranslate(0, 0, 0.05)
        glutSolidSphere(0.1, 30, 30)
        glColor(0.2, 0.2, 0.2)
        glRotate(90, 0, 0, 1)
        self.tes(0.14, 0, 0,0.1, 1, 1,0.3)
        glPopMatrix()

    def cilindroFininho(self):
        glPushMatrix()
        glColor(0.3, 0.3, 0.3)
        glutSolidCylinder(0.02, 1.5, 30, 4)
        glRotate(-90, 0, 1, 0)
        # pé 1
        self.tranlateCilinder(0.1, 0, 0,0.02, 0.1, 30, 4)
        # pé 2
        self.tranlateCilinder(1.3, 0, 0,0.02, 0.1, 30, 4)
        glPopMatrix()

    def vagao(self):
        glColor(0.1, 0.1, 0.1)
        glPushMatrix()  # meio frente
        self.tes(0,0,0,0.5, 0.01, 1,1.6)
        self.tes(1.3, 0, 0,0.1, 0.01, 1,1.6)   # direita frente
        self.tes(-26, 0, 0,1,1,1,1.6) #esquerda frente
        glPopMatrix()
        self.direita()
        glTranslate(-1.3, 0, 0)#esquerda
        self.direita()
        glPushMatrix() #tras
        glRotate(90, 0, 1, 0)
        self.tes(0, -1.6, 1.34,1.2, 0.01, 1,1.32)
        glPopMatrix()
        self.teto()

        #baixo
        glPushMatrix()
        # glColor(0.1, 0.1, 0.1)
        self.tes(1.34, -0.8, .8,0.8, 1, 0.01,1.6)
        glPopMatrix()

        glPushMatrix()
        glRotate(90, 0, 1, 0)
        glRotate(90, 1, 0, 0)
        glTranslate(-0.8, 1.305, 0)
        glutSolidCylinder(0.25, 1, 40, 20)
        glPopMatrix()
        #baixo2
        glPushMatrix()
        self.tes(1.3, 0.01, 0.5,0.85, 0.01, 0.38,1.6)
        glPopMatrix()

    def teto(self):
        glPushMatrix()
        glColor(0.2, 0.2, 0.2)
        glRotate(90, 1, 0, 0)
        glTranslate(1.3, -0.8, -0.2)
        glScale(1, 0.2, 1)
        glutSolidCylinder(0.8, 2, 40, 40)
        glPopMatrix()

    def direita(self):
        glPushMatrix() # direita
        glRotate(90, 0, 1, 0)
        self.tes(-0.4, -0.8, 0.68,0.5, 1, 0.01,1.6)
        glPopMatrix()
        glPushMatrix()
        glRotate(90, 0, 0, 1)
        self.tes(-0.8, -0.65, 0,0.1, 0.01, 1,1.6)
        glPopMatrix()

    def girar(self, z, w, a,b,c):
        glTranslate(a,b,c)
        glPushMatrix()
        glTranslate(0, z, w)
        if (self.y < 0.07):
            self.y += 0.01
        elif (self.y == 0.07):
            temp1 = 0
            while (temp1 < 7):
                self.y -= 0.01
                temp1 += 1
        glTranslate(0, self.y, self.y)
        self.retaDasRodas()
        glPopMatrix()

    def vagao(self):
        glColor(0.1, 0.1, 0.1)
        glPushMatrix() # meio frente
        self.tes(0,0,0,0.5, 0.01, 1,1.6)
        self.tes(1.3, 0, 0,0.1, 0.01, 1,1.6)# direita frente
        self.tes(-26, 0, 0,0,0,0,1.6)#esquerda frente
        glPopMatrix()
        self.direita()
        glTranslate(-1.3, 0, 0)#esquerda
        self.direita()
        self.trasVagao(0,0,0)#tras
        self.teto()

        glPushMatrix()#baixo
        glColor(0.1, 0.1, 0.1)
        self.tes(1.34, -0.8, .8,0.8, 1, 0.01,1.6)
        glPopMatrix()

        glPushMatrix()
        glRotate(90, 0, 1, 0)
        glRotate(90, 1, 0, 0)
        self.tranlateCilinder(-0.8, 1.305, 0,0.25, 1, 40, 20)
        glPopMatrix()
        glPushMatrix()#baixo2
        self.tes(1.3, 0.01, 0.5,0.85, 0.01, 0.38,1.6)
        glPopMatrix()

    def carga(self, x,y,z):
        glTranslate(x,y,z)
        glPushMatrix()
        glColor(0.1,0.1,0.1)
        self.tes(0,0,0,.01, 1, 2,1.6)
        glPopMatrix()

    def trasVagao(self, x,y,z):
        glTranslate(x,y,z)
        glPushMatrix()
        glRotate(90, 0, 1, 0)
        self.tes(0, -1.6, 1.34,1.2, 0.01, 1,1.32)
        glPopMatrix()	
			
