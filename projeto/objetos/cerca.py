from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
from OpenGL.GLUT.freeglut import glutWireSierpinskiSponge


class Cerca(object):
    def __init__(self):
        pass
    def haste(self):
        glPushMatrix()
        glColor3f(190/255.0, 190/255.0, 190/255.0)
        glTranslate(0.0, 2.0, 0.0)
        glRotate(90, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.03, 2.5, 20, 20)
        glPopMatrix()
    def haste_horizontal(self, espessura):
        glPushMatrix()
        glRotate(90, 0.0, 1.0, 0.0)
        glutSolidCylinder(espessura, 3.1, 20, 20)
        glPopMatrix()
    def arame(self):
        glPushMatrix()
        glColor3f(0.6, 0.6, 0.6)
        glTranslate(0.1, 0.25, 0.0)
        glScale(0.0001, 1.0, 2.7)
        glutWireCylinder(1.0, 1.1, 30, 50)
        glPopMatrix()
    def arame_superior(self):
        glPushMatrix()
        glTranslate(0.1, 2.1, 0.0)
        glRotate(90, 0.0, 1.0, 0.0)
        #glScale(0.15, 0.25, 0.1)
        glutWireCylinder(0.3, 3.0, 10, 30)
        glPopMatrix()

    def desenhar(self):
        glPushMatrix()
        glTranslate(0.0, 0.0, 0.0)
        self.haste()
        glTranslate(3.0, 0.0, 0.0)
        self.haste()
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.04, -0.2, 0.0)
        self.haste_horizontal(0.05)
        glTranslate(0.0, 1.0, 0.0)
        glColor3f(110/255.0, 123/255.0, 139/255.0)
        self.haste_horizontal(0.08)
        glTranslate(0.0, 1.0, 0.0)
        glColor3f(190/255.0, 190/255.0, 190/255.0)
        self.haste_horizontal(0.05)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, 0.5, 0.1)
        glRotate(90.0, 0.0, 1.0, 0.0)
        self.arame()
        glPopMatrix()

        glPushMatrix()
        #for i in range(0, 30):
        self.arame_superior()
        #    glTranslate(0.1, 0.0, 0.0)
        glPopMatrix()