#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Aula sobre composição de objetos e uso do teclado.
from math import cos
from math import pi
from math import sin
import timeit
#import numpy
import ctypes
import random
from sys import argv
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from OpenGL.GLUT.freeglut import glutSolidCylinder
from funcoes import *

    
def desenho():
    bicicleta()

def bicicleta(x,y,z):
    glPushMatrix()
    glScale(x,y,z)

    #pneu1
    glColor3f(0.0, 0.0, 0.0)
    glPushMatrix()
    glutSolidTorus(0.05,0.3,100,100) 
    glPopMatrix()      
    
    # OBJETO raio1
    glColor3f(0.0, 0.0, 0.0) 
    glPushMatrix()  
    rtsc(90, 1.0, 0.0, 0.0,0.0, 0.0, -0.3,0.009, 0.6, 5, 5)  
    glPopMatrix()

    # OBJETO raio2
    glColor3f(0.0, 0.0, 0.0) 
    glPushMatrix()    
    rtsc(90, 0.0, 1.0, 0.0,0.0, 0.0, -0.3,0.009, 0.6, 5, 5)  
    glPopMatrix()

    #ferro do pneu1
    glColor3f(1.0, 0.0, 0.0) 
    glPushMatrix()                
    glRotatef(180, 0, 1, 1) 
    rtsc(30, 0, 1, 0,0,0,0,0.03, 0.7, 20, 5)  
    glPopMatrix()
    
    #ferro 2
    glColor3f(1.0, 0.0, 0.0) 
    glPushMatrix()                
    glRotatef(180, 0, 1, 1) 
    rtsc(90, 0, 1, 0,-0.4, 0, 0.22,0.03, 0.7, 20, 5)  
    glPopMatrix()
 
    #ferro 3
    glColor3f(1.0, 0.0, 0.0) 
    glPushMatrix()                
    glRotatef(90, 1, 0, 0) 
    rtsc(-45, 0, 1, 0,-0.46, 0, -0.12,0.03, 0.7, 20, 5)  
    glPopMatrix()    
    
    #bolinhaDoPedal
    glPushMatrix()
    glColor3f(0.0, 0.0, 0.0)
    rtsc(0,0,0,0,-0.8, 0 ,0.01,0.15,0.02,20,20)  
    glPopMatrix() 
    
    #bolinhaBrancaDoPedal
    glPushMatrix()
    glColor3f(1.0, 1.0, 1.0)
    rtsc(0,0,0,0,-0.8, 0 ,0.02,0.05,0.02,20,20)  
    glPopMatrix() 
    
    #ferro 4
    glColor3f(1.0, 0.0, 0.0) 
    glPushMatrix()  
    rtsc(90, 1, 0, 0,-0.9,0,-0.6,0.022, 0.7, 20, 5)  
    glPopMatrix() 

    #ferro 5
    glColor3f(1.0, 0.0, 0.0) 
    glPushMatrix()                
    glRotatef(90, 1, 0, 0) 
    rtsc(-45, 0, 1, 0,-0.9,0,0.3,0.03, 0.63, 20, 5)  
    glPopMatrix() 
  
    # OBJETO raio1'
    glColor3f(0.0, 0.0, 0.0) 
    glPushMatrix()    
    rtsc(90, 1.0, 0.0, 0.0,-1.3, 0.0, -0.3,0.009, 0.6, 5, 5)  
    glPopMatrix()
    
    # OBJETO raio2
    glColor3f(0.0, 0.0, 0.0) 
    glPushMatrix() 
    rtsc(90, 0.0, 1.0, 0.0,0.0, 0.0, -1.6,0.009, 0.6, 5, 5)  
    glPopMatrix()
    
    #guidon
    glColor3f(1.0, 0.0, 0.0) 
    glPushMatrix() 
    rtsc(0,0,0,0,-0.25,0.64,-0.25,0.03, 0.5, 20, 5)  
    glPopMatrix()
    
    #guidonPunho1
    glColor3f(0.0, 0.0, 0.0) 
    glPushMatrix()  
    rtsc(0,0,0,0,-0.25,0.64,-0.35,0.03, 0.1, 20, 5)  
    glPopMatrix()
        
    #guidonPunho2
    glColor3f(0.0, 0.0, 0.0) 
    glPushMatrix()  
    rtsc(0,0,0,0,-0.25,0.64,0.25,0.03, 0.1, 20, 5)  
    glPopMatrix()
    
    #ferro do guidon
    glColor3f(1.0, 0.0, 0.0) 
    glPushMatrix()                
    glRotatef(180, 0, 1, 1) 
    rtsc(-60, 0, 1, 0,0.68, 0, -0.02,0.03, 0.1, 20, 5)  
    glPopMatrix()
    
    #ferro 5
    glColor3f(1.0, 0.0, 0.0) 
    glPushMatrix()                
    glRotatef(90, 1, 0, 0)
    rtsc(90, 0, 1, 0,-0.05,0,-1.3,0.03, 0.63, 20, 5)  
    glPopMatrix()
        
    #pneu2
    glColor3f(0.0, 0.0, 0.0)
    glPushMatrix()
    glTranslate(-1.3,0,0)
    glutSolidTorus(0.05,0.3,100,100) 
    glPopMatrix() 
     
    #bolinhaDoPneu1
    glPushMatrix()
    glColor3f(1.0, 0.0, 0.0)
    glutSolidCylinder(0.10,0.02,20,20)  
    glPopMatrix()

    #bolinhaDoPneu2
    glPushMatrix()
    glColor3f(1.0, 0.0, 0.0)
    rtsc(0,0,0,0,-1.3,0,0,0.10,0.02,20,20)  
    glPopMatrix()
    
    #bolinha1
    glPushMatrix()
    glColor3f(0.0, 0.0, 0.0)
    glutSolidTorus(0.05,0.05,10,19)
    glPopMatrix()
    
    #bolinha2
    glPushMatrix()
    glColor3f(0.0, 0.0, 0.0)
    glTranslate(-1.3,0,0)
    glutSolidTorus(0.05,0.05,10,19)
    glPopMatrix()
    
    #banco
    glPushMatrix()
    glColor3f(0.0, 0.0, 0.0)
    glRotate(90,0,1,0)
    glRotate(90,1,0,0)
    glTranslate(0,-0.9,-0.6)
    glutSolidTorus(0.10,0.06,10,40)
    glPopMatrix()    
    glPopMatrix()
