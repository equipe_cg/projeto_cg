# -*-coding: utf-8 -*
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

class Trilhos(object):
    i = 0
    rodando = 0
    rodando2 = True

    def desenhar(self):
        if self.rodando2:
            j=0
            while j<7:
                glTranslate(0,0,-6)
                self.trilhos()
                j+=1
        self.trilhos() #trilhos da frente
        glPushMatrix()
        glTranslate(0,0,-6)
        self.trilhos() #trilhos de tras
        glTranslate(0,0,-7)
        self.trilhos() #trilhos de tras
        glPopMatrix()

        
    def trilhos(self):
        glPushMatrix()
        glTranslate(0,-1.17,-2)
        glColor(0.3,0.2,0)
        glScale(1,0.09,0.2)
        temp = 0
        while temp < 12:
            glTranslate(0,0,3)
            temp+=1
            glutSolidCube(1.4)

        glPopMatrix()
        glPushMatrix()
        glTranslate(0.3,-1.09,2.5)
        self.trilho1()
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.3,-1.09,2.5)
        self.trilho1()
        glPopMatrix()

    def trilho1(self):
        glPushMatrix()
        glColor(0.4,0.4,0.4)
        glScale(0.02,0.02,2)
        glutSolidCube(4)
        glPopMatrix()
