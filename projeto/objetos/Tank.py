from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
from math import cos
from math import sin
from math import pi

class Tank:
    def __init__(self, r, g, b):
        self.animacao = 1.0
        self.angulo = 0.0
        self.z = 0.5
        self.x = 0.0
        self.velocidade = 0.1
        self.direcao = 0.0
        self.r = r
        self.g = g
        self.b = b
        self.habilitar = True
        self.colisao=[0,0,0,0]
    def cabeca(self):
        glPushMatrix()
        glRotate(90, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.3, 0.3, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.14, 0.3)
        glScale(0.5, 0.26, 0.3)
        glutSolidCube(1)
        glPopMatrix()

        #entrada
        glPushMatrix()
        glTranslate(0.1, 0.05, -0.1)
        glRotate(90, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.12, 0.05, 64, 64)
        glPopMatrix()

        #cano
        glPushMatrix()
        glTranslate(0.0, -0.13, 0.4)
        glutSolidCylinder(0.1, 0.35, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.13, 0.7)
        glutSolidCylinder(0.07, 0.4, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.13, 1.0)
        glutSolidCylinder(0.04, 0.7, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.13, 1.7)
        glutSolidTorus(0.012, 0.04, 64,64)
        glPopMatrix()

    def placas(self):

        glPushMatrix()
        glColor3f(0.2, 0.2, 0.2)
        glRotate(90, 0.0, 1.0, 0.0)
        for i in range(0, 2):
            glTranslate(-0.02, 0.0, 0.0)
            glutSolidCylinder(0.02, 0.27, 10, 1)
        glPopMatrix()

    def pneu(self):
        glColor3f(self.r, self.g, self.b)
        glPushMatrix()
        glRotate(90, 0.0, 1.0, 0.0)
        glTranslate(0.0, 0.0, 0.0)
        glutSolidCylinder(0.2, 0.2, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glRotate(90, 0.0, 1.0, 0.0)
        glTranslate(0.0, 0.0, 0.0)
        glutSolidCylinder(0.22, 0.08, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.12, 0.0, 0.0)
        glRotate(90, 0.0, 1.0, 0.0)
        glutSolidCylinder(0.22, 0.08, 64, 64)
        glPopMatrix()

    def anima1(self):
        glPushMatrix()
        glTranslate(0.27, -0.5, 1.0)
        glRotate(180, 0.0, 1.0, 0.0)
        glRotate(25.0, 1.0, 0.0, 0.0)
        for i in range(0, 8):
            glTranslate(0.0, 0.0, -0.06)
            glRotate(12.0, 1.0, 0.0, 0.0)
            self.placas()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.5, -0.2)
        glRotate(25.0, 1.0, 0.0, 0.0)
        for i in range(0, 8):
            glTranslate(0.0, 0.0, -0.06)
            glRotate(12.0, 1.0, 0.0, 0.0)
            self.placas()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.5, 1.0)
        for i in range(0, 20):
            glTranslate(0.0, 0.0, -0.06)
            self.placas()
        glPopMatrix()

    def anima2(self):
        glPushMatrix()
        glTranslate(0.27, -0.5, 0.9)
        glRotate(180, 0.0, 1.0, 0.0)
        glRotate(5.0, 1.0, 0.0, 0.0)
        for i in range(0, 10):
            glTranslate(0.0, 0.0, -0.06)
            glRotate(12.0, 1.0, 0.0, 0.0)
            self.placas()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.5, -0.25)
        glRotate(35.0, 1.0, 0.0, 0.0)
        for i in range(0, 10):
            glTranslate(0.0, 0.0, -0.06)
            glRotate(12.0, 1.0, 0.0, 0.0)
            self.placas()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.5, 0.9)
        for i in range(0, 19):
            glTranslate(0.0, 0.0, -0.06)
            self.placas()
        glPopMatrix()

    def anima3(self):
        glPushMatrix()
        glTranslate(0.27, -0.5, 0.8)
        glRotate(180, 0.0, 1.0, 0.0)
        glRotate(-15.0, 1.0, 0.0, 0.0)
        for i in range(0, 12):
            glTranslate(0.0, 0.0, -0.06)
            glRotate(12.0, 1.0, 0.0, 0.0)
            self.placas()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.5, -0.28)
        glRotate(40.0, 1.0, 0.0, 0.0)
        for i in range(0, 12):
            glTranslate(0.0, 0.0, -0.06)
            glRotate(12.0, 1.0, 0.0, 0.0)
            self.placas()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, -0.5, 0.8)
        for i in range(0, 18):
            glTranslate(0.0, 0.0, -0.06)
            self.placas()
        glPopMatrix()

    def pneuRodas(self):
        glPushMatrix()
        z = -0.5
        glTranslate(0.035, 0.0, 0.01)
        for i in range(0, 3):
            self.pneu()
            glTranslate(0.0, 0.0, z)
        glPopMatrix()
        glPushMatrix()
        glTranslate(0.0, 0.26, -0.9)
        if self.animacao >= 1.0 and self.animacao < 2.0:
            self.anima1()
        elif self.animacao == 2 and self.animacao < 3.0:
            self.anima2()
        else:
            self.anima3()

        glPopMatrix()

    def protetor(self):
        #parte lado direito
        glPushMatrix()
        glTranslate(0.55, -0.18, 0.35)
        glRotate(2, 1.0, 0.0, 0.0)
        glRotate(90, 0.0, 0.0, 1.0)
        glScale(0.05, 0.314,1.55)
        glutSolidCube(1)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.55, -0.3, 1.18)
        glRotate(-40, 1.0, 0.0, 0.0)
        glScale(0.314, 0.2, 0.02)
        glutSolidCube(1)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.55, -0.28, -0.48)
        glRotate(30, 1.0, 0.0, 0.0)
        glScale(0.314, 0.25, 0.02)
        glutSolidCube(1)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.7, -0.33, 0.35)
        glRotate(2, 1.0, 0.0, 0.0)
        glScale(0.01, 0.25,1.5)
        glutSolidCube(1)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.7, -0.36, 1.1)
        glRotate(49, 1.0, 0.0, 0.0 )
        glScale(0.01, 0.2, 0.2)
        glutSolidCube(1)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.7, -0.32, -0.387)
        glRotate(30, 1.0, 0.0, 0.0 )
        glScale(0.01, 0.25, 0.2)
        glutSolidCube(1)
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.3, 0.0, 0.0)
        glPushMatrix()
        glTranslate(0.7, -0.33, 0.35)
        glRotate(2, 1.0, 0.0, 0.0)
        glScale(0.01, 0.25,1.5)
        glutSolidCube(1)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.7, -0.36, 1.1)
        glRotate(49, 1.0, 0.0, 0.0 )
        glScale(0.01, 0.2, 0.2)
        glutSolidCube(1)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.7, -0.32, -0.387)
        glRotate(30, 1.0, 0.0, 0.0 )
        glScale(0.01, 0.25, 0.2)
        glutSolidCube(1)
        glPopMatrix()
        glPopMatrix()

    def corpo(self):
        #parte superior
        glPushMatrix()
        glTranslate(0.0, 0.0, 0.4)
        glRotate(2, 1.0, 0.0, 0.0)
        glScale(1.0, 0.3, 1.5)
        glutSolidCube(1)
        glPopMatrix()

        self.protetor()

        #parte lado esquerdo
        glPushMatrix()
        glTranslate(-1.12, 0.0, 0.0)
        self.protetor()
        glPopMatrix()


        #cilindro da frente
        glPushMatrix()
        glTranslate(-0.25, -0.45, 1.25)
        glRotate(90, 0.0, 1.0, 0.0)
        glutSolidCylinder(0.02, 0.5, 64, 64)
        glPopMatrix()

        #esfera
        glPushMatrix()
        glTranslate(-0.3, -0.015, 1.15)
        glutSolidTorus(0.02, 0.06, 64, 64)
        glPopMatrix()

        #parte inferior
        glPushMatrix()
        glTranslate(0.0, -0.35, 0.4)
        glScale(0.8, 0.45, 1.2)
        glutSolidCube(1)
        glPopMatrix()

        #tapa buraco
        glPushMatrix()
        glScale(0.5, 0.2, 0.4)
        glTranslate(0.0, -1.28, 2.65)
        glRotate(50, 1.0, 0.0, 0.0)
        glutSolidCube(1.0)
        glPopMatrix()

        glPushMatrix()
        glScale(0.5, 0.18, 0.4)
        glTranslate(0.0, -1.9, 2.76)
        glRotate(15, 1.0, 0.0, 0.0)
        glutSolidCube(1.0)
        glPopMatrix()

        glPushMatrix()
        glScale(0.5, 0.2, 0.4)
        glTranslate(0.0, -2.15, 2.45)
        glRotate(40, 1.0, 0.0, 0.0)
        glutSolidCube(1.0)
        glPopMatrix()

        #detalhes
        glPushMatrix()
        glTranslate(0.35, 0.3, -0.4)
        glRotate(90, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.07, 0.3, 64, 64)
        glTranslate(-0.16, 0.0, 0.0)
        glutSolidCylinder(0.07, 0.3, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.0, 0.19, -0.43)
        glRotate(2, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.025, 0.55, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.025, 0.19, -0.4)
        glRotate(90, 0.0, 1.0, 0.0)
        glutSolidCylinder(0.025, 0.3, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.46, 0.2, -0.3)
        glRotate(90, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.01, 0.1, 64, 64)
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.46, 0.95, -0.435)
        glRotate(80, 1.0, 0.0, 0.0)
        glutSolidCylinder(0.002, 0.8, 64, 64)
        glPopMatrix()

    def desenhar(self):


        glColor3f(self.r, self.g, self.b)

        glPushMatrix()
        glTranslate(self.x, 0, self.z)
        glPushMatrix()
        glRotate(self.direcao, 0, 1, 0)
        glPushMatrix()
        glRotate(-90, 0.0, 1.0, 0.0)

        glPushMatrix()
        glTranslate(0.0, 0.38, 0.4)
        glRotate(self.angulo, 0.0, 1.0, 0.0)
        self.cabeca()
        glPopMatrix()
        self.corpo()

        glPushMatrix()
        glTranslate(0.40, -0.45, 0.85)
        self.pneuRodas()
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.7, -0.45, 0.85)
        self.pneuRodas()
        glPopMatrix()
        glPopMatrix()
        glPopMatrix()
        glPopMatrix()


    def animacao_(self):
        if self.animacao < 3:
            self.animacao += 0.5
        else:
            self.animacao = 1.0
    def frente(self):
        xt=self.x
        zt=self.z
        xt-=cos(self.direcao * pi/180) * self.velocidade
        zt+=sin(self.direcao * pi/180) * self.velocidade
        #self.x=self.confirma_movimentos_x(xt)

        self.confirma_movimentos_x(xt)
        self.confirma_movimentos_z(zt)
        self.animacao_()
    def tras(self):
        xt=self.x
        zt=self.z
        xt+=cos(self.direcao * pi/180) * self.velocidade
        zt-=sin(self.direcao * pi/180) * self.velocidade

        self.confirma_movimentos_x(xt)
        self.confirma_movimentos_z(zt)
        self.animacao_()
    def esquerda(self):
        self.direcao = (self.direcao + 2.0) % 360.0
    def direita(self):
        self.direcao = (self.direcao - 2.0) % 360.0
    def girarDireita(self):
        if self.angulo > -90:
            self.angulo -= 1
    def girarEsquerda(self):
        if self.angulo < 90:
            self.angulo += 1


    def confirma_movimentos_x(self,valor):
        if self.colisao[0]:
            if valor<self.x:
                self.x=valor
        elif self.colisao[1]:
            if valor>self.x:
                self.x=valor
        else:
            self.x=valor

    def confirma_movimentos_z(self,valor):
        if self.colisao[3]:
            if valor<self.z:
                self.z=valor
        elif self.colisao[2]:
            if valor>self.z:
                self.z=valor
        else:
            self.z=valor