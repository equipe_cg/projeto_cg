# -*-coding: utf-8 -*

#Allan Lucio Correia
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from math import cos
from math import pi
from math import sin
from math import pi
import random
from bicicleta import bicicleta


class Alojamento(object):
    cor_corpo=[0.8,0.3,0.3]
    pos_x=0
    def desenhar(self):
        glPushMatrix()
        glTranslate(-0.7,-0.07,1)
        glRotate(-90,0,1,0)
        bicicleta(0.3,0.3,0.3)
        glPopMatrix()

        glPushMatrix()
        self.centro()
        glPopMatrix()
	
    def centro(self):
        glPushMatrix()
        self.corpo()
        self.janela(0.32,0.25,0)
        self.janela(-0.32,0.25,0)
        self.porta(0,0.1,0)
        self.cerca(0.6,0,1)
        self.telha()
        glPopMatrix()
			
    def corpo(self):
        glPushMatrix()
        glColor3f(0.4,0.4,0.4)
        glTranslate(0,0,-2)
        glutSolidCylinder(0.65,3,30,30)

        """
        glPushMatrix()

        glColor3f(0.5,0.5,0.5)
        glTranslate(0,-0.2,1.5)
        glPushMatrix()
        glScale(1,0.01,1)
        glutSolidCube(4)
        glPopMatrix()
        glPopMatrix()
        """
        glPopMatrix()

    def janela(self,x,y,z):
        glPushMatrix()
        glTranslate(0+x,0+y,1+z)

        glPushMatrix()
        glScale(0.5,1,0.01)
        glutSolidCube(0.25)
        glPopMatrix()
        glPopMatrix()
    def porta(self,x,y,z):
        glPushMatrix()
        glTranslate(0+x,0+y,1+z)

        glPushMatrix()
        glScale(0.6,1.5,0.01)
        glutSolidCube(0.35)

        glPopMatrix()
        glPopMatrix()

    def estaca(self,x,y,z,tamanho=0.5):
        glPushMatrix()
        glColor3f(0.6,0.6,0.6)
        glTranslate(x,y,z)

        glPushMatrix()
        glScale(0.05,0.5,0.1)
        glutSolidCube(tamanho)
        glPopMatrix()


        glPopMatrix()

    def cerca(self,x,y,z):
        glPushMatrix()
        glTranslate(x,y,z)
        glPushMatrix()
        for a in range(0,10):
            self.estaca(a/20.0,0,0)
        glRotate(90,0,0,1)
        glPushMatrix()

        self.estaca(0.08,-0.22,0,1)
        self.estaca(-0.08,-0.22,0,1)
        glPopMatrix()
        glPopMatrix()
        glPopMatrix()

    def telha(self):

        glTranslate(0,  0.3,1.1)

        glPushMatrix()
        glRotate(90,1,0,0)
        glRotate(35,1,0,0)
        glPushMatrix()
        glScale(0.5,0.05,0.0001)
        self.estaca(0.005,-0.09,0,10.5)
        glPopMatrix()

        glPopMatrix()
