#!/usr/bin/env python
# -*- coding: utf-8 -*-
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *	

def rtsc(a,b,c,d,e,f,g,h,i,j,k):
        glPushMatrix()  
	glRotatef(a,b,c,d)     
	glTranslate(e,f,g)
	glutSolidCylinder(h, i, j, k)  
    	glPopMatrix()

