# -*-coding: utf-8 -*

# Allan Lucio Correia
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from math import cos
from math import pi
from math import sin
from math import pi
import random
from OpenGL.raw.GLUT import glutSolidIcosahedron


class Aviao(object):
    cor_corpo = [0.8, 0.3, 0.3]
    ligar = False
    velocidade_helice = 0
    limite_velocidade=0.5
    angulo_helice = 0
    posicao_animacao = 0
    posicao_animacao_translate = 0
    anima_x = [0, 0]
    anima_y = [0, 0]
    anima_z = [0, 0]
    pos_x = -10

    def __init__(self,tipo_voo=1):
        self.cor_corpo = [0, 0.0, 0.3]
        self.ligar = False
        self.velocidade_helice = 0
        self.angulo_helice = 0
        self.posicao_animacao = 0
        self.posicao_animacao_translate = 0
        self.anima_x = [0, 0]
        self.anima_y = [0, 0]
        self.anima_z = [0, 0]
        self.pos_x = -15
        self.pos_y = -2
        self.pos_z = 0
        self.limite_velocidade=0.07
        self.tipo_voo=tipo_voo


    def desenhar(self):

        #self.centro()




        #animação translação



        self.pos_x += self.velocidade_helice*self.limite_velocidade


        self.posicao_animacao_translate += 0.5
        glTranslate(self.pos_x, self.pos_y, 0)
        glPushMatrix()
        if self.velocidade_helice > 1.9:
            self.anima_voo(self.tipo_voo)

            self.organiza_animacao()


        self.central()
        self.asas()
        self.trem_de_pouso()
        self.animacao()
        glPopMatrix()

    def anima_voo(self,num):
        if num ==1:
            self.pos_y=sin(self.rad(self.posicao_animacao_translate * self.velocidade_helice)) * 5
        if num ==2:
            self.pos_y=((self.velocidade_helice-1.9)*13)-2

    #metade do centro
    def centro(self):
        glPushMatrix()
        glTranslate(-1.5, 0, 0)

        glPushMatrix()
        glRotate(90, 0, 1, 0)
        o=gluNewQuadric()
        gluQuadricDrawStyle(o,GLU_FILL)
        gluCylinder(o,0.33,0.6,3.17,10,10)
        #eixo Z

        #glScale(0.4, 0.7, 1)

        #raio = 0.5
        #while raio < 1:
         #   glTranslate(0, 0, 0.05)
         #   glutSolidCylinder(raio, 0.05, 30, 30)
         #   raio += 0.008

        glPopMatrix()

        glPopMatrix()
    def set_color(self,r,g,b):
        self.cor_corpo=[r,g,b]
    #desenho de uma Asa

    def trem_de_pouso(self):
        glPushMatrix()
        glTranslate(0.8,-0.9,-1)
        self.roda()
        glTranslate(0,0,1.9)
        glPushMatrix()
        glRotate(180,0,1,0)

        self.roda()
        glPopMatrix()
        glPopMatrix()

    def roda(self):
        glPushMatrix()
        glColor3f(0,0,0)
        glutSolidTorus(0.1,0.25,20,20)


        glTranslate(0,0.15,0)
        glPushMatrix()
        glColor3f(self.cor_corpo[0], self.cor_corpo[1], self.cor_corpo[2])
        glScale(0.4,0.3,0.4)
        glutSolidSphere(1,10,10)
        glPopMatrix()
        glPopMatrix()

        glPushMatrix()
        glTranslate(0.18,0,0)
        glPushMatrix()
        glRotate(-60,1,0,0)
        glutSolidCylinder(0.05,1,10,10)
        glPopMatrix()
        glPopMatrix()

        glPushMatrix()
        glTranslate(-0.3,0,0)
        glPushMatrix()
        glRotate(-60,1,0,0)
        glutSolidCylinder(0.05,1,10,10)
        glPopMatrix()
        glPopMatrix()


    def asa(self):
        glPushMatrix()
        glScale(1, 0.10, 1)
        raio = 1
        while raio > 0.3:
            glTranslate(0, 0, 0.03)
            glPushMatrix()

            glutSolidCylinder(raio, 0.3, 30, 30)
            glPopMatrix()
            raio -= 0.007

        glPopMatrix()

    #Centro do avião, corpo!
    def central(self):
        self.cabine()
        glColor3f(self.cor_corpo[0], self.cor_corpo[1], self.cor_corpo[2])
        glPushMatrix()
        glPushMatrix()
        glTranslate(-1, 0, 0)
        self.centro()
        self.asa_trazeira()
        glPopMatrix()
        glRotate(180, 0, 0, 1)
        glTranslate(-1.5, 0, 0)

        glPushMatrix()
        glScale(0.5, 1, 1)
        self.centro()
        glPopMatrix()

        glPopMatrix()

        self.bico()

    #Junção e rotação das asas
    def asas(self):
        glPushMatrix()
        #self.asa()
        #glRotate(180, 1, 0, 0)
        #self.asa()
        glScale(0.7,0.1,3)
        glutSolidIcosahedron(2)

        glPopMatrix()

    #Desenho completo do bico junto com a helice
    def bico(self):
        glPushMatrix()
        glTranslate(2.3, 0, 0)
        glutSolidSphere(0.2, 50, 50)
        glPushMatrix()
        glRotate(self.angulo_helice, 1, 0, 0)

        self.angulo_helice += 15 * self.velocidade_helice
        if self.angulo_helice >= 180:
            self.angulo_helice = 0
        self.helices()
        glPopMatrix()
        glPopMatrix()

    #desenho individual da hélice
    def helice(self):
        glPushMatrix()

        glPushMatrix()
        glRotate(90, 0.0, 1.0, 0.0)
        glScale(6, 0.5, 0.1)

        glutSolidSphere(0.2, 50, 50)

        glPopMatrix()
        glPopMatrix()

    #Junção das duas helices e organização do angulo
    def liga_helice(self, ligar):
        if self.velocidade_helice <= 0:
            self.anima_x = [0, 0]
            self.anima_y = [0, 0]
            self.anima_z = [0, 0]
        self.ligar = ligar

    def animacao(self):
        if self.ligar:
            self.add_velicidade_helice(0.01)

        else:

            self.remove_velicidade_helice(0.015)

    def add_velicidade_helice(self, vel):

        if self.velocidade_helice <= 2.6:
            self.velocidade_helice += vel

    def remove_velicidade_helice(self, vel):

        if self.velocidade_helice > 0:
            self.velocidade_helice -= vel


    def helices(self):
        glPushMatrix()
        self.helice()
        glRotate(90, 1, 0, 0)
        self.helice()
        glPopMatrix()

    def asa_trazeira(self):
        glPushMatrix()
        glTranslate(-1.4, 0, 0)
        self.bico_trazeiro()

        glPushMatrix()
        glScale(1, 0.1, 5)
        glutSolidSphere(0.20, 50, 50)

        glPopMatrix()

        glTranslate(-0.34, 0.1, 0)
        glPushMatrix()
        #glRotate(90, 0, 1, 0)
        glRotate(90, 1, 0, 0)

        #dimenções da esfera achada da asa
        glScale(0.40,0.1,0.9)
        glutSolidIcosahedron(1)
        #glutSolidSphere(0.25, 50, 50)
        glPopMatrix()

        glPopMatrix()

    def bico_trazeiro(self):
        glPushMatrix()

        glPushMatrix()
        glRotate(-90, 0, 1, 0)
        glScale(0.4, 0.7, 0.7)
        glutSolidCone(0.5, 1.0, 40, 40)
        glPopMatrix()

        glPopMatrix()

    #Cabine do piloto
    def cabine(self):
        glPushMatrix()
        glTranslate(0, 0.45, 0)

        glPushMatrix()
        glRotate(90, 0, 1, 0)
        glRotate(90, 0, 0, 1)
        glScale(0.45, 0.4, 1)
        glutWireCylinder(0.75, 1, 10, 5)

        glColor4f(0.5, 0.5, 0.5, 0.1)
        glutSolidCylinder(0.75, 1, 20, 20)

        glPopMatrix()
        glPopMatrix()

    #transforma graus em radianos
    def rad(self, x):
        return pi * x / 180

    #mudança de animação e previsão da proxima
    def muda_animacao(self, variavel):
        if variavel == 'x':
            self.anima_x[0] = self.anima_x[1]
            self.anima_x[1] = random.randint(-100, 100) / 100.0
        elif variavel == 'y':
            self.anima_y[0] = self.anima_y[1]
            self.anima_y[1] = random.randint(-100, 100) / 100.0
        else:
            self.anima_z[0] = self.anima_z[1]
            self.anima_z[1] = random.randint(-100, 100) / 100.0

    #organiza a animação nos varios eixos, e faz a previsão da proxima para suavizar as trocas
    def organiza_animacao(self):
        #Calculo do ceno para que gire no eixo corretamente
        vx = sin(self.rad(self.posicao_animacao * self.anima_x[0] * self.velocidade_helice))
        vy = sin(self.rad(self.posicao_animacao * self.anima_y[0] * self.velocidade_helice))
        vz = sin(self.rad(self.posicao_animacao * self.anima_z[0] * self.velocidade_helice))

        #valor da previsão da proxima troca
        vxprox = sin(self.rad(self.posicao_animacao * self.anima_x[1] * self.velocidade_helice))
        vyprox = sin(self.rad(self.posicao_animacao * self.anima_y[1] * self.velocidade_helice))
        vzprox = sin(self.rad(self.posicao_animacao * self.anima_z[1] * self.velocidade_helice))

        #Suavizando as trocas, só troca quando chega num valor próximo ao posterios
        if int(vx * 360) == int(vxprox * 360):
            #carrega um novo valor aleatorio para animar
            self.muda_animacao('x')
        if int(vy * 360) == int(vyprox * 360):
            #carrega um novo valor aleatorio para animar
            self.muda_animacao('y')
        if int(vz * 360) == int(vzprox * 360):
            #carrega um novo valor aleatorio para animar
            self.muda_animacao('z')
        #coeficiente de velocidade da animação
        self.posicao_animacao += 0.7
        #Função para girar nos eixos, o valor vx*valor é quem determina até quantos graus ele gira
        glRotate(vx * 60, 1, 0, 0)
        #glRotate(vy * 120, 0, 1, 0)
        #glRotate(vz * 200, 0, 0, 1)
			
