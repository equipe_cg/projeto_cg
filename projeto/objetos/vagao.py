#!/usr/bin/env python
# -*- coding: utf-8 -*-
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

class Vagao(object):

	def desenhar(self):
		glColor(0.1,0.1,0.1)
		glPushMatrix() 
		glTranslate(0,0,-3.3)
        	glScale(0.5, 0.5, 1)
		glutSolidCube(3)
	
		glPopMatrix()
