# /usr/bin/env python
# -*-coding: utf-8 -*
#Nome: Allan Lucio Correia

from math import cos
from math import pi
from math import sin
import timeit
import time
import thread
#import numpy
import ctypes
import random
from sys import argv
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLU.quadrics import gluNewQuadric
from OpenGL.GLUT import *
from OpenGL.raw.GLE import gleHelicoid


class Canhao(object):
        def __init__(self):
            self.posicao=[0,0,0]
            self.cor=[0,0,0]
        def desenhar(self):
            glPushMatrix()
            glTranslate(self.posicao[0],self.posicao[1],self.posicao[2])
            glColor3f(self.cor[0],self.cor[1],self.cor[2])

            glPushMatrix()

            self.canhao()
            glPopMatrix()

            glPopMatrix()

        def colisao(self):
            return ''
        def muda_posicao(self,x=0,y=0,z=0):
            self.posicao[0]+=x
            self.posicao[1]+=y
            self.posicao[2]+=z
        def muda_cor(self,x,y,z):
            self.cor=[x,y,z]
            

        #Desenhos
        def canhao(self):
            glPushMatrix()
            self.base()
            glPushMatrix()



            glRotate(-20,0,0,1)
            glPushMatrix()
            glTranslate(0,0.3,0.35)
            self.arma()
            glPopMatrix()
            glPopMatrix()
            glPopMatrix()


        def base(self):

            #o=gluNewQuadric()
            #gluQuadricDrawStyle(o,GLU_LINE)
            #gluCylinder(o,0.5,1.5,1.5,50,20)

            #gluPartialDisk(o,0,1.5,20,20,0,60)
            #gluSphere(o,1.3,32,32)
            #gluDisk(o,0.5,1.5,32,32)
            #glScale(1,0.1,2)
            #glutSolidIcosahedron(1) #Usar na Asa do aviao
            glPushMatrix()
            glTranslate(-0.8,0,0)
            glScale(0.7,0.7,0.7)
            self.roda()
            glPushMatrix()
            glTranslate(0,0,1)

            self.roda()
            glPopMatrix()
            glutSolidCylinder(0.1,1,15,15)

            glPushMatrix()
            glScale(1,0.1,1)
            glTranslate(0,0,0.5)
            glutSolidCube(0.8)
            glPopMatrix()

            glPushMatrix()
            glTranslate(0,0.4,0.2)

            glPushMatrix()
            glScale(1,1,0.15)

            glutSolidCube(0.8)


            glutSolidCube(0.8)
            glPopMatrix()

            glTranslate(0,0,0.6)
            glPushMatrix()
            glScale(1,1,0.15)

            glutSolidCube(0.8)


            glutSolidCube(0.8)
            glPopMatrix()
            glPopMatrix()
            glPopMatrix()


        def roda(self):


            glPushMatrix()

            glPushMatrix()
            glScale(1,1,0.7)
            glColor3f(0,0,0)
            glutSolidTorus(0.15,0.6,30,30)
            glPopMatrix()

            glPopMatrix()

            glPushMatrix()
            glPushMatrix()
            glColor(0.5,0.4,0.2)
            glScale(1,1,0.4)
            glutSolidSphere(0.2,8,8)
            glPopMatrix()

            glPushMatrix()
            glRotate(90,1,0,0)
            glColor(0.5,0.5,0)
            for a in range(0,6):

                glRotate(60,0,1,0)
                glPushMatrix()
                glutSolidCylinder(0.03,0.5,10,10)
                glutSolidCylinder(0.05,0.23,10,10)

                glTranslate(0,0,0.34)
                glutSolidCylinder(0.05,0.20,10,10)
                glPopMatrix()
            glPopMatrix()
            glPopMatrix()

        def arma(self):
            glPushMatrix()
            glRotate(-90,0,1,0)


            glPushMatrix()
            glPushMatrix()
            glRotate(-180,0,1,0)
            glPushMatrix()
            glTranslate(0,0,0.18)
            glutSolidSphere(0.06,25,25)
            glPopMatrix()

            glutSolidCone(0.2,0.2,25,25)
            glPopMatrix()

            glColor3f(1,1,0)
            glutSolidTorus(0.02,0.2,30,30)
            glColor3f(0,0,0)
            glutSolidCylinder(0.2,0.7,20,20)


            glTranslate(0,0,0.7)
            glColor3f(1,1,0)
            glutSolidTorus(0.015,0.19,30,30)

            glColor3f(0,0,0)
            glutSolidCylinder(0.19,0.8,20,20)

            glTranslate(0,0,0.8)
            glutSolidCylinder(0.18,0.8,20,20)

            for a in range(0,3):

                glColor3f(1,1,0)
                glutSolidTorus(0.01,0.18,30,30)
                glTranslate(0,0,0.19)




            glTranslate(0,0,0.2)
            glPushMatrix()
            glRotate(180,0,1,0)
            #glutSolidCone(0.23,0.23,25,25)
            glPopMatrix()
            glColor3f(0.5,0.5,0.5)
            glutSolidCylinder(0.15,0.04,20,20)
            glPopMatrix()
            glPopMatrix()