# encoding:utf-8

"""
Autor: Douglas
W p/ frente
S p/ tras
Q e E p/ girar canhão
"""
from OpenGL.GLUT.special import glutTimerFunc

from objetos.Tank import *
from objetos.Torre import *
from cenario.Cenario import *
from objetos.alojamento import *
from objetos.trem import *
from objetos.aviao import *
from objetos.canhao import *
from objetos.trilhos import *
from objetos.tunel import *
from objetos.barricada import *


#======================
#variaveis da camera
from objetos.explosao import *


xpos = 0.0
zpos = 0.0
xrot = 0.0
yrot = 0.0
zrot = -1.0
angulo = 40.0

camera = 2
CAMERA_UM = 0 #camera superior direita
CAMERA_DOIS = 1 #seque o tank
CAMERA_TRES = 2 #camera de cima
CAMERA_QUATRO = 3 #Melhor camera
#=====================
tank = Tank(0.0, 0.3, 0.0)
tank2 = Tank(0.0, 0.3, 0.0)
tank3 = Tank(138/255.0, 121/255.0, 30/255.0)


aviao = Aviao()

aviao.set_color(138/255.0, 121/255.0, 30/255.0)
aviao2 = Aviao(2)
aviao2.liga_helice(True)
aviao2.set_color(0.0, 0.3, 0.0)

trem_anda=False

torre = Torre(0, 0, 0)
alojamento = Alojamento()
canhao = Canhao()
trem = Trem()
trilhos = Trilhos()
tunel = Tunel()
barricada = Barricada()
tempo=0
quadro=0
var=0
obj=0
obj2=0
obj3=0
lista_teclas=[]
ex=Explosao(1,[0,2,0])

x=0
y=0
z=0
tam=0.2
#x,z,tamx,tamz,0
lcol=[[13.8,-4.8,0.1,4,0],[-23.6,-5.4,0.2,50,0],[18.0,-5.4,1,50,0],[-3.0,-23.8,40,2,0],[-3.0,13.4,40,2,0],[-21.6,-6.0,0.5,21,0],[-9.0,-17.0,25,0.5,0],[-9.0,7.0,25,0.5,0],[5.4,-12.4,0.7,11,0],[5.0,2.2,0.2,9.2,0],[-14.8,2.0,2.35,2.35,0],[-14.2,-1.2,3.6,3.6,0],[-17.0,-1.0,2.55,2.55,0],[-15.0,-7.2,5.15,5.15,0],[-14.0,-12.0,3.45,3.45,0]]
def desenhar():


    glPushMatrix()
    glTranslate(-15.0, 0.0, 3.0)
    glRotate(180, 0.0, 1.0, 0.0)
    tank2.desenhar()
    glPopMatrix()

    # if tank3.habilitar:
    #     glPushMatrix()
    #     glTranslate(18.0, 0.0, -7.0)
    #     glRotate(120, 0.0, 1.0, 0.0)
    #     glRotate(180, 0.0, 1.0, 0.0)
    #     tank3.desenhar()
    #     glPopMatrix()

    glPushMatrix()
    glTranslate(-10.0, 0.0, -10.0)
    desenhor()
    glPopMatrix()

    glPushMatrix()
    glTranslate(-15.0, 1.4, -7.0)
    glScale(4.0, 4.0, 4.0)
    torre.desenhar()
    glPopMatrix()

    glPushMatrix()
    glTranslate(-13.5, 3.7, -8.0)
    glRotate(-50, 0.0, 1.0, 0.0)
    glScale(0.5, 0.5, 0.5)
    canhao.desenhar()
    glTranslate(3.0, 0.0, 2.0)
    glRotate(-80, 0.0, 1.0, 0.0)
    canhao.desenhar()
    glPopMatrix()

    glPushMatrix()
    glTranslate(-10.0, -0.35, -10.0)
    glScale(2.0, 2.0, 2.0)

    glPushMatrix()

    glTranslate(7.0, 0.2, -3)
    glScale(0.3, 0.3, 0.3)

    glPushMatrix()
    glTranslate(22, 0, 44)
    trilhos.desenhar()
    glPopMatrix()


    glPopMatrix()
    glPopMatrix()

    glPushMatrix()
    glTranslate(-15.0, -0.35, -12.0)
    glRotate(90, 0.0, 1.0, 0.0)
    glScale(2.0, 2.0, 2.0)
    alojamento.desenhar()
    glTranslate(-5.5, 0.0, 0.0)
    alojamento.desenhar()
    glPopMatrix()



    glPushMatrix()
    glTranslate(17.0, 0.0, -26)
    tunel.desenhar()
    glPopMatrix()

    glPushMatrix()
    glTranslate(17.0, 0.0, 10)
    tunel.desenhar()
    glPopMatrix()

    #pista do avião
    glPushMatrix()
    glTranslate(-10.0, -0.9, -5.5)
    glScale(2.0, 0.5, 22.0)
    glutSolidCube(1)
    glPopMatrix()

    #barricada
    glPushMatrix()
    glTranslate(12.0, -0.2, -5.25)
    glRotate(90, 0.0, 1.0, 0.0)
    barricada.desenhar()
    glPopMatrix()

def tank_redesenhar():
    global obj3
    glNewList(obj3, GL_COMPILE)
    glPushMatrix()
    tank.desenhar()


    glPopMatrix()
    glEndList()
def desenhar2():
    global obj2,trem_anda


    glNewList(obj2, GL_COMPILE)
    ex.desenhar()


    if trem_anda:
        glPushMatrix()
        glTranslate(17.2, .05, -20)
        glScale(0.61,0.61,0.61)
        trem.desenhar()
        if trem.trem_andar(0.3) == 0:
            trem_anda=False

        glPopMatrix()


    glPushMatrix()
    glTranslate(-100, 17, 0.0)
    aviao.desenhar()
    #canhao.desenhar()
    glPopMatrix()

    #Aviao por enquanto deixei aqui
    glPushMatrix()
    glTranslate(-10.0, 2.55, 2.0)
    glRotate(-90, 0.0, 1.0, 0.0)
    #glScale(1.0, 1.0, 1.0 )
    aviao2.desenhar()
    glPopMatrix()


    glPushMatrix()

    glTranslate(7.0, 0.2, -3)
    glScale(0.3, 0.3, 0.3)
    glPopMatrix()

    glEndList()

def iluminacao():

    glFogi(GL_FOG_MODE, GL_EXP)
    glFogfv(GL_FOG_COLOR, [0.5, 0.5, 0.5, 1.0])
    glFogf(GL_FOG_DENSITY, 0.03)
    glHint(GL_FOG_HINT, GL_DONT_CARE)
    glFogf(GL_FOG_START, 1.0)
    glFogf(GL_FOG_END, 5.0)
    glEnable(GL_FOG)

    LuzAmbiente = [0.5, 0.5, 0.5, 1.0]
    LuzDifusa = [0.4, 0.4, 0.4, 1.0]
    LuzEspecular = [1.0, 1.0, 1.0, 1.0]
    PosicaoLuz = [1000.0, 100.0, -1000.0, 1.0]
    Especularidade = [1.0, 1.0, 1.0, 1.0]

    glEnable(GL_LIGHTING)

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LuzAmbiente)

    glLightfv(GL_LIGHT0, GL_AMBIENT, LuzAmbiente)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, LuzDifusa)
    glLightfv(GL_LIGHT0, GL_SPECULAR, LuzEspecular)
    glLightfv(GL_LIGHT0, GL_POSITION, PosicaoLuz)
    glEnable(GL_LIGHT0)

    glEnable(GL_COLOR_MATERIAL)

    #glMaterialfv(GL_FRONT, GL_SPECULAR, Especularidade)

    #glMateriali(GL_FRONT, GL_SHININESS, 20)

    glEnable(GL_BLEND)

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_DEPTH_TEST)


def display():
    global tempo,quadro,var
    tempo=int(time.clock())

    quadro+=1
    if tempo==var:
        #print "fps:",quadro
        quadro=0
        var+=1

    global angulo, xrot, yrot, zrot
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # Limpar a tela  |

    #glEnable(GL_CULL_FACE)
    #glCullFace(GL_BACK)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    gluPerspective(angulo, 1, 0.1, 500)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    if camera == CAMERA_UM:
         gluLookAt(-1, 5, -9, 1, 0, 0.2, 0, 1, 0)
        #gluLookAt(-5, 15, -5, 0, -2, 0.2, 0, 1, 0)
        #angulo = 05
    elif camera == CAMERA_DOIS:
        gluLookAt(xpos + 0.3, 0.1, zpos + 0.05, xrot, yrot, zrot, 0, 1, 0)
    elif camera == CAMERA_TRES:
        gluLookAt(16, 15, 0, 0, 1, -0.6, -1.0, 1, 0)
        #angulo = 8
    ############### Camera correta (nao apaguem) ##########################
    else:
        camPos = [tank.x + 2 * cos(tank.direcao * pi/180), 0.9, tank.z + 2 * sin(-tank.direcao * pi/180)]
        camTarget = [tank.x, 0.75 + yrot, tank.z]
        gluLookAt(camPos[0]/10.0, camPos[1]/10.0, camPos[2]/10.0, camTarget[0]/10.0, camTarget[1]/10.0, camTarget[2]/10.0, 0, 1, 0)
        #angulo = 2
    ########################################################
    glScale(0.1, 0.1, 0.1)
    glCallList(obj)
    glCallList(obj2)
    glCallList(obj3)
    global x,y,z,tam
    glPushMatrix()
    glTranslate(x,y,z)
    glutSolidCube(tam)

    glPopMatrix()

    for col in lcol:
        col[4]=colisao(col[0],col[1],col[2],col[3],col[4])

    #print tank.colisao
    #print tank.x
    desenhar2()
    movimenta()
    glutSwapBuffers()




def inserir(tecla):
    if tecla not in lista_teclas:
        lista_teclas.append(tecla)





def keyboard(tecla,x, y):

    inserir(tecla)


def keyboardUp(tecla,x, y):
    global lista_teclas
    lista_teclas.remove(tecla)


def especialKeyboard(key, x, y):

    global yrot, zrot
    if key == GLUT_KEY_UP:
        yrot += 0.5

    if key == GLUT_KEY_DOWN:
        yrot -= 0.5

    if key == GLUT_KEY_LEFT:
        zrot += 1.0
    if key == GLUT_KEY_RIGHT:
        zrot -= 1.0
    display()
    glutPostRedisplay()


def processMouse(button, state, x, y):
    global angulo
    if state == GLUT_UP:
        if button == 3:
            if angulo > 2:
                angulo -= 2
        elif button == 4:
            angulo += 2

    glutPostRedisplay()

def movimenta():
    global xpos, zpos, xrot, zrot, angulo,lista_teclas,ex,x,y,z,tam,trem_anda
    for tecla in lista_teclas:

        tank_redesenhar()


        velocidade = 0.01
        xrot = tank.x
        zrot = tank.z
        if tecla.upper() == 'W':


            tank.frente()

            xpos -= cos(tank.direcao * pi/180) * velocidade
            zpos += sin(tank.direcao * pi/180) * velocidade
        if tecla.upper() == 'S':
            tank.tras()

            xpos += cos(tank.direcao * pi/180) * velocidade
            zpos -= sin(tank.direcao * pi/180) * velocidade
        if tecla.upper() == 'A':
            tank.esquerda()

        if tecla.upper() == 'D':
            tank.direita()

        if tecla.upper() == 'E':
            tank.girarDireita()
        if tecla.upper() == 'Q':
            tank.girarEsquerda()

        if tecla.upper() == 'B':


            ex.animacao=1
            ex.tamanho=250
            tiroPos = [tank.x - 20 * cos((tank.angulo+tank.direcao) * pi/180), 0.9, tank.z -20  * sin(-(tank.direcao+tank.angulo) * pi/180)]
            ex.posicao=[tiroPos[0],tiroPos[1],tiroPos[2]]

        if tecla.upper() == 'I':
            x+=0.2
        if tecla.upper() == 'K':
            x-=0.2

        if tecla.upper() == 'J':
            y-=0.2
        if tecla.upper() == 'L':
            y+=0.2
        if tecla.upper() == 'U':
            z-=0.2

        if tecla.upper() == 'O':
            z+=0.2

        if tecla.upper() == 'M':
            tam-=0.05

        if tecla.upper() == 'N':
            tam+=0.05
        if tecla.upper() == 'T':
            trem_anda=True

        print 'posicao= [%s,%s,%s] tamanho= %s'%(x,y,z,tam)


def colisao(x1,z1,l,c,col):
    x=tank.x
    z=tank.z

    #print tank.direcao
    l=l/2+2
    c=c/2+1
    vet=[0,0,0,0]
    direc=tank.direcao
    # print direc
    x-=cos((direc*3.14/180.0))
    z+=sin((direc*3.14/180.0))

    # glPushMatrix()
    # glTranslate(x,0,z)
    # glutSolidCube(2)
    # glPopMatrix()

    if int(direc)==90 or int(direc)==270:
        l,c=c,l

    if modulo(x - x1) < l and modulo(z - z1) < c:
        resultadox = x - x1
        resultadoz = z - z1

        # bloqueia tecla A e L
        if resultadox >= -l and resultadox < 0:
            vet[0]=1

        #bloqueia tecla D e J
        elif resultadox <= l and resultadox > 0:
            vet[1]=1
        #bloqueia tecla W e K
        if resultadoz <= c and resultadoz > 0:
            vet[2]=1
        #bloqueia tecla S e I
        elif resultadoz >= -c and resultadoz < 0:
            vet[3]=1

        tank.colisao=vet
        return 1
    else:
        if col==1:
            tank.colisao=[0,0,0,0]
        return 0


def modulo(valor):
	if valor <0:
		valor*=-1
	return valor

def EnquantoEspera():
    global rodando, trem, aviao
    global lista_teclas
    global xpos, zpos, xrot, zrot, angulo,lista_teclas


    aviao.liga_helice(True)
    aviao.animacao()
    glutPostRedisplay()


def init():
    global obj,obj2,obj3
    glClearColor(135 / 255.0, 206 / 255.0, 250 / 255.0, 1.5)
    iluminacao()
    obj=glGenLists(1)
    glNewList(obj, GL_COMPILE)

    desenhar()


    glEndList()
    obj2=glGenLists(2)
    obj3=glGenLists(3)
    tank_redesenhar()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(800, 600)
    glutInitWindowPosition(200, 100)
    glutCreateWindow("Coffee Duty - A Ameaça Dourada")
    glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF)
    glutDisplayFunc(display)
    glutIdleFunc(EnquantoEspera)
    glutKeyboardFunc(keyboard)
    glutKeyboardUpFunc(keyboardUp)
    glutSpecialFunc(especialKeyboard)
    glutMouseFunc(processMouse)
    init()
    glutMainLoop()


main()
